#!/bin/bash
YESTERDAY=`date +%Y.%m.%d`
ELK_URL=172.111.0.1:9200

#YESTERDAY=2022.09.19

PREFIX=prod-packetbeat-appserver-6.3.2
INDEX=${PREFIX}-${YESTERDAY}

> uniq_log.txt
#> output_raw.txt

UNIQ_ID=`cat result_${INDEX}.txt | jq .[].\"X-Unistream-Security-PosId\" | sort -n | uniq`

IDX=`cat result_${INDEX}.txt | jq . | jq keys[]`
for i in ${IDX}
do
#  echo "index-${i}"
    arr=`cat result_prod-packetbeat-appserver-6.3.2-2022.09.19.txt | jq .[${i}]`
    id=`echo $arr | jq .\"X-Unistream-Security-PosId\"`
    ip=`echo $arr | jq .\"IP\"`
    echo "${id} ${ip}" >> uniq_log.txt
done

for z in ${UNIQ_ID}
do
#check for doubles
counter=`grep ${z} output_raw.txt | wc -l`
b=1
if [ "${counter}" -gt "$b" ]
then
awk '{last[$1]=$0;} END{for (i in last) print last[i];}' output_raw.txt > recreate.txt
cat recreate.txt > output_raw.txt
rm -f recreate.txt
#  else
#    echo "No doubles"
fi
########################

  ips=`awk '{last[$1]=$0;} END{for (i in last) print last[i];}' output_raw.txt | grep ${z} | awk '{print $2}'`

  for list in $(cat uniq_log.txt | grep ${z} | awk '{print $2}')
  do
#    echo "ips=$ips"
#echo "list=$list"
    if [ "${ips}" = "" ]; then
         ips=${list}
    elif grep -q "${list}" <<< "${ips}"; then
    ips=${ips}
    else
        ips=${ips},${list}
    fi
#    if [ "${ips}" = "" ]; then
#
#    fi

#echo $list
    done
    ips_old=`awk '{last[$1]=$0;} END{for (i in last) print last[i];}' output_raw.txt | grep ${z} | awk '{print $2}'`
    if [ "${ips}" = "${ips_old}" ]; then
   echo "ok"
    else
      echo "${z}: ${ips}" >> output_raw.txt
      awk '{last[$1]=$0;} END{for (i in last) print last[i];}' output_raw.txt > recreate.txt
      cat recreate.txt > output_raw.txt
    fi
  done
  # Прогнать в самом конце на последнее вхождение, собрать массив и записать только массив в файл

rm -f uniq_log.txt

exit 0