# - Скрипт search.sh собирает статистику за вчера и кладет в массив json dump_records.txt 

# -  convert_stat.sh - разбирает на список адресов по айдишнику

# На всякий случай я так и не понял объебался я или нет, но если не будет искать в эластике замени в переменной ALL_RECORD скрипа search.sh :
``        "source.real_ip",
"source.request",``

на :

``        "real_ip",
"request",``

# Выходные файлы: 
- dump_records.txt - Все данные без обработи за вчера
- result_prod-packetbeat-appserver-6.3.2-${YESTERDAY}.txt - json массив с 2 полями
- output_raw.txt - списки адресов по айдишникам 

# Всякие полезности чтоб локально воспроизвести стенд:

`+RkC6n++W+-bXQ110xBR - elk pass (у тебя будет другой)`

`curl --insecure -X PUT "https://localhost:9200/prod-packetbeat-appserver-6.3.2-2022.09.19?pretty" -u elastic:+RkC6n++W+-bXQ110xBR -- Создать индекс

curl -H 'Content-Type: application/json' --insecure -XPOST 'https://172.111.0.1:9200/prod-packetbeat-appserver-6.3.2-2022.09.19/_doc/_bulk?pretty' --data-binary @example.json -u elastic:+RkC6n++W+-bXQ110xBR -- влить пример

```
curl -H 'Content-Type: application/json' --insecure -X POST 'https://172.111.0.1:9200/prod-packetbeat-appserver-6.3.2-2022.09.19/_search' -d '
{
"query": {
"match_all": {}
},
"fields": [
"source.real_ip",
"source.request",         
{
"field": "@timestamp",
"format": "epoch_millis"
}
],
"_source": false
}' -u elastic:+RkC6n++W+-bXQ110xBR | jq .hits.hits[].fields  -- общий список 


curl -H 'Content-Type: application/json' --insecure -X POST 'https://172.111.0.1:9200/prod-packetbeat-appserver-6.3.2-2022.09.19/_search' -d '
{
"query": {
"match_all": {}
},
"fields": [
"source.real_ip",
"source.request",         
{
"field": "@timestamp",
"format": "epoch_millis"
}
],
"_source": false
}' -u elastic:+RkC6n++W+-bXQ110xBR | jq .hits.hits[0].fields | jq 'keys'[] -- только ключи

{
"query": {
"match_all": {}
},
"fields": [
"source.real_ip",
"source.request",         
{
"field": "@timestamp",
"format": "epoch_millis"
}
],
"_source": false
}' -u elastic:+RkC6n++W+-bXQ110xBR | jq .hits.hits | jq keys -- порядковый номер записи в эластике

