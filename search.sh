#!/bin/bash
echo "[" > work.txt
YESTERDAY=`date +%Y.%m.%d`
ELK_URL=172.111.0.1:9200

#YESTERDAY=2022.09.19

PREFIX=prod-packetbeat-appserver-6.3.2
INDEX=${PREFIX}-${YESTERDAY}

echo ${INDEX}
echo ${ELK_URL}

ALL_RECORD=`curl -H 'Content-Type: application/json' --insecure -X POST 'https://'${ELK_URL}'/'${INDEX}'/_search' -d '
        {
        "query": {
        "match_all": {}
        },
        "fields": [
        "source.real_ip",
        "source.request",
        {
        "field": "@timestamp",
        "format": "epoch_millis"
        }
        ],
        "_source": false
        }' -u elastic:+RkC6n++W+-bXQ110xBR | jq .hits.hits > dump_records.txt`
echo "dump all records"
#cat dump_records.txt
for i in  $(cat dump_records.txt | jq keys[])
do
  for z in $i
  do
    ID=`cat dump_records.txt | jq .[${z}].fields.\"source.request\"[] | grep -oE "X-Unistream-Security-PosId: [0-9]{1,}" | sed 's/\:/":/'`
    IP=`cat dump_records.txt | jq .[${z}].fields.\"source.real_ip\"[]`
    ID_1=`echo ${ID} | awk '{print $1 " \""}'`
    ID_2=`echo ${ID} | awk '{print $2}'`
    ID="${ID_1}${ID_2}"
    echo "{
    \"${ID}\",
\"IP\": ${IP}
}," >> work.txt
    done
  done
sed -e '$s/,$/\]/' work.txt > result_${INDEX}.txt && rm -f work.txt
